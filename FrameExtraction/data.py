import os 
import numpy as np
from sklearn.model_selection import train_test_split

training_set = np.empty((11407), dtype=str)
labels = {}

i = 0
for dir in os.listdir(r'C:\Users\Study\Documents\TennisProject\FrameExtraction\TrainingSet\NotServe'):
    training_set[i] = dir
    labels = {dir : 0}
    i+=1

for dir in os.listdir(r'C:\Users\Study\Documents\TennisProject\FrameExtraction\TrainingSet\Serve'):
    training_set[i] = dir
    labels = {dir : 1}
    i+=1

X_train, X_val = train_test_split(training_set, test_size=.2)

partitions = {'Training': X_train, "Validation" : X_val}

np.save("training_set.npy", partitions)
np.save("labels.npy", labels)
