import cv2 as cv
import os
import numpy as np

video = cv.VideoCapture('tennismatch1.mp4')
strokes = {'Dead': [], 'ForehandV' : [], 'ForehandB' : [], 'BackhandB' : [], 'BackhandV' : [], 'Serve' : [], 'OverheadV' : []}

dead = False
f_v = False
f_b = False
b_b = False
b_v = False
s = False
o = False
fast = False
pause = False

def saveImage(letter):
    global dead, f_v, f_b, b_b, b_v, s, fast, pause, o
    if letter == ord('f'):
        f_b = True
        dead, f_v, b_b, b_v, s, o = False, False, False, False, False, False
    elif letter == ord('s'):
        s = True
        dead, f_v, b_b, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('j'):
        b_b = True
        dead, f_v, s, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('d'):
        dead = True
        b_b, f_v, s, b_v, f_b, o = False, False, False, False, False, False 
    elif letter == ord('g'):
        f_v = True
        b_b, dead, s, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('h'):
        b_v = True
        b_b, f_v, s, f_b, o, dead = False, False, False, False, False, False
    elif letter == ord('o'):
        o = True
        b_b, f_v, s, b_v, f_b, dead = False, False, False, False, False, False
    elif letter == ord('w'):
        if fast:
            fast = False
        else:
            fast = True
    elif letter == ord('q'):
        dead, b_b, f_v, s, f_v, f_b, b_v, o = False, False, False, False, False, False, False, False
    elif letter == ord('p'):
        if pause:
            pause = False
        else:
            pause = True
            dead, b_b, f_v, s, f_v, f_b, b_v, fast, o = False, False, False, False, False, False, False, False, False




while(video.isOpened()):
    isTrue, frame = video.read()

    cv.imshow('Video', frame)
    if pause:
        letter = cv.waitKey(0)
    else:
        if fast:
            letter = cv.waitKey(5)
        else:
            letter = cv.waitKey(70)
    saveImage(letter)

    if dead:
        strokes.get('Dead').append(frame)
        print('D')
    elif f_v:
        strokes.get('ForehandV').append(frame)
        print('FV')
    elif f_b:
        strokes.get('ForehandB').append(frame)
        print('FB')
    elif b_b:
        strokes.get('BackhandB').append(frame)
        print('BB')
    elif b_v:
        strokes.get('BackhandV').append(frame)
        print('BV')
    elif s:
        strokes.get('Serve').append(frame)
        print('S')
    elif o:
        strokes.get('OverheadV').append(frame)
        print('O')
    else:
        print('Q')

    if letter == ord('c'):
        break

path = r'C:\Users\Study\Documents\TennisProject\FrameExtraction\TrainingSet'

i = 5000
for stroke in strokes:
    for img in strokes.get(stroke):
        cv.imwrite(os.path.join(path, stroke, 'stroke{0}.jpg'.format(i)), img)
        i += 1

video.release()
cv.destroyAllWindows()
