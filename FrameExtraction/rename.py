import cv2
import os

i = 11000
path = r"C:\Users\Study\Documents\TennisProject\FrameExtraction\TrainingSet\Serve"
for img in os.listdir(path):
    image = cv2.imread(os.path.join(path, img))
    cv2.imwrite(os.path.join(path, 'stroke{0}.jpg'.format(i)), image)
    i +=1
