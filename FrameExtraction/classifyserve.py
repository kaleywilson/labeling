# USAGE
# python yolo.py --image images/baggage_claim.jpg --yolo yolo-coco

# import the necessary packages
from os import listdir
import numpy as np
import argparse
import time
import cv2 as cv
import os

wholeset = np.array(0)
shape2 = 0
path = r'C:\Users\Study\Desktop\labeling\FrameExtraction\TrainingSet'
notserve = ['Dead', 'ForehandV', 'ForehandB', 'BackhandB', 'BackhandV', 'OverheadV']
labels = []
images = np.ndarray((12181,720,1280,3), dtype=np.uint8)
count = 0
def load_images_from_folder(folder, isServe):
    global images
    global labels
    global count
    for filename in os.listdir(folder):
        img = cv.imread(os.path.join(folder,filename))
        if img is not None:
            images[count] = img
            count+=1
            if isServe:
                labels.append("Serve")
            else:
                labels.append("Not Serve")
    return images

for folder in notserve:
    load_images_from_folder(os.path.join(path,"OverheadV"), False)


load_images_from_folder(os.path.join(path,"OverheadV"), True)

print(len(labels))
#print(len(images))
print(labels)
