import numpy as np
from tensorflow import keras
from keras.models import Sequential
from my_classes import DataGenerator

# Parameters
params = {'dim': (32,32,32),
          'batch_size': 64,
          'n_classes': 2,
          'n_channels': 1,
          'shuffle': True}

# Datasets
partition = np.load("training_set.npy", allow_pickle=True)
labels = np.load("labels.npy", allow_pickle=True)

# Generators
training_generator = DataGenerator(partition['Training'], labels, **params)
validation_generator = DataGenerator(partition['Validation'], labels, **params)

# Design model
model = Sequential()
model.add(keras.layers.Dense(500, activation="relu"))
model.add(keras.layers.Dense(200, activation="relu"))
model.add(keras.layers.Dense(2, activation = "softmax"))
model.compile()

# Train model on dataset
model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True,
                    workers=2)